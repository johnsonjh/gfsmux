module github.com/johnsonjh/gfsmux

go 1.19

require github.com/johnsonjh/leaktestfe v0.0.0-20221210113806-1ad56057a826

require go.uber.org/goleak v1.2.0 // indirect
